package com.ruben.utils;

import android.app.Activity;
import android.os.Build;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: MyActivityManager
 * @Description: 我还没有写描述
 * @Date: 2020/10/18 0018 10:16
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class MyActivityManager {

    public static final List<Activity> activityList = Collections.synchronizedList(new LinkedList<>());

    public static void addActivity(Activity activity) {
        activityList.add(activity);
    }

    public static void removeActivity(Activity activity) {
        activityList.remove(activity);
    }

    public static Activity getActivity() {
        if (isActivityEmpty()) {
            return null;
        }
        return activityList.get(activityList.size() - 1);
    }

    public static void killActivity() {
        killActivity(getActivity());
    }

    public static void killActivity(Activity activity) {
        if (activity == null) {
            return;
        }
        if (isActivityEmpty()) {
            return;
        }
        activity.finish();
        removeActivity(activity);
    }

    public static void killActivity(Class<?> cls) {
        Activity activity = getActivity(cls);
        if (activity == null) {
            return;
        }
        killActivity(activity);
    }


    public static Activity getActivity(Class<?> cls) {
        if (isActivityEmpty()) {
            return null;
        }
        if (cls == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return null;
        }
        List<Activity> activities = activityList.stream().filter(activity -> activity.getClass().equals(cls)).collect(Collectors.toList());
        if (activities.isEmpty()) {
            return null;
        }
        return activities.get(0);
    }

    public static Activity getTopActivity() {
        Activity activity = null;
        synchronized (activityList) {
            final int index = activityList.size() - 1;
            if (index < 0) {
                return null;
            }
            activity = activityList.get(index);
        }
        return activity;
    }

    public static String getTopActivityName() {
        Activity topActivity = getTopActivity();
        if (topActivity == null) {
            return null;
        }
        return topActivity.getClass().getName();
    }

    public static void killAllActivity() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return;
        }
        activityList.forEach(Activity::finish);
        activityList.clear();
    }

    private static boolean isActivityEmpty() {
        return activityList.isEmpty();
    }

}
