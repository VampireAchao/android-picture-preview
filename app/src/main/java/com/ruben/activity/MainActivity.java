package com.ruben.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.imagepreview.R;

public class MainActivity extends AppCompatActivity {

    //读写权限
    public static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    //请求状态码
    public static int REQUEST_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, REQUEST_PERMISSION_CODE);
            }
        }
        findViewById(R.id.driveCar).setOnClickListener(v -> {
            startActivity(new Intent(this, MainImageListActivity.class));
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                Log.i("MainActivity", "申请的权限为：" + permissions[i] + ",申请结果：" + grantResults[i]);
            }
        }
    }

    //    private void oldInit() {
//        // 打开时默认选择第几张
//        int position = 0;
//        // 图片路径list
//        List<String> photoUrls = new ArrayList<>();
//        photoUrls.add("https://vampireachao.gitee.io/imgs/preview/6671_3.jpg");
//        photoUrls.add("https://vampireachao.gitee.io/imgs/preview/3291_3.jpg");
//        photoUrls.add("https://vampireachao.gitee.io/imgs/preview/6307_3.jpg");
//        Intent intent = new Intent(getApplicationContext(), PicturePreviewActivity.class);
//        // 传入默认打开的图片位置
//        intent.putExtra(PicturePreviewActivity.IMAGE_NUM, position);
//        // 传入图片路径
//        intent.putExtra(PicturePreviewActivity.IMAGES_DATA_LIST, (Serializable) photoUrls);
//        // 开始活动，如果已存在于一个为它运行的任务中，就把它移到最前
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        getApplication().startActivity(intent);
//    }
}