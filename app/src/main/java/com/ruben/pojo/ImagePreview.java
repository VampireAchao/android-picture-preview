package com.ruben.pojo;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @ClassName: ImagePreview
 * @Description: 我还没有写描述
 * @Date: 2020/10/17 0017 0:59
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ImagePreview implements Serializable {
    private static final long serialVersionUID = 1299428728791895273L;
    private Integer id;
    private String url;
    private Date createDate;
    private Date updateDate;
    private String source;
    private String category;
}
