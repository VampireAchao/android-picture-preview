package com.ruben.interfaces;

/**
 * @ClassName: ListDataListener
 * @Description: 我还没有写描述
 * @Date: 2020/10/17 0017 1:55
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public interface ListDataListener {
    void loadMore();

    void refresh();
}
