package com.ruben.dao;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

/**
 * @ClassName: MySQLiteOpenHelper
 * @Description: 我还没有写描述
 * @Date: 2020/10/18 0018 22:46
 * *
 * @author: <achao1441470436@gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class MySQLiteOpenHelper extends SQLiteOpenHelper {
    // 相当于 SQLiteDatabase openDatabase(String, CursorFactory)
    public MySQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // 相当于 SQLiteDatabase openDatabase(String, CursorFactory, DatabaseErrorHandler)
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public MySQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    // 相当于 SQLiteDatabase openDatabase(String , OpenParams);
    @TargetApi(Build.VERSION_CODES.P)
    public MySQLiteOpenHelper(Context context, String name, int version, SQLiteDatabase.OpenParams openParams) {
        super(context, name, version, openParams);
    }

    // 创建数据文件时调用，此时适合创建新表
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists favorite(" +
                "`id` INTEGER PRIMARY KEY AUTOINCREMENT," +
                "`url` text not null," +
                "`createDate` date," +
                "`updateDate` date," +
                "`source` varchar(100)," +
                "`category` varchar(5))";
        db.execSQL(sql);
    }

    // 更新数据库版本时调用，适合更新表结构或创建新表
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
